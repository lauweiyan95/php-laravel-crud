<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

# Simple CRUD

## Configure Database

1. cd to project
2. Open .env file
3. Modify the database connection
4. Type command `php artisan migrate`

## How to start

1. Type command `php artisan serve`
2. When the development server is up, open the link http://127.0.0.1:8000/employees
